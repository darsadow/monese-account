# README

Sorry for the lack of unit tests, I really feel bad about it. 

But there is `TheIntegrationTest` covering both endpoint for both accounts. 

The code is written to be easily testable.

Provider Classes are used for ease of mocking generated values. As alternative a Spy can be used.

## Running

```
./gradlew bootRun
```

## Tests

I've used `testcontainers` so there is no need for additional setup.

```
./gradlew test
```


