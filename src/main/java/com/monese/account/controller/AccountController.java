package com.monese.account.controller;

import com.monese.account.model.controller.AccountTransferRequest;
import com.monese.account.model.controller.StatementResponse;
import com.monese.account.model.domain.Money;
import com.monese.account.service.AccountService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.UUID;

@RestController
@RequestMapping("/accounts")
public class AccountController {

    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/{accountId}/statement")
    public StatementResponse accountStatement(@PathVariable("accountId") UUID accountId) {
        return accountService.accountStatement(accountId);
    }

    @PostMapping("/transfer")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void transfer(@Valid @RequestBody AccountTransferRequest request) {
        Money transferAmount = new Money(BigDecimal.valueOf(request.getAmount()), request.getCurrency());

        accountService.transfer(
                request.getSourceAccountId(),
                request.getDestinationAccountId(),
                transferAmount,
                request.getReference()
        );
    }
}
