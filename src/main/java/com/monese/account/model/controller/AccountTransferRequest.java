package com.monese.account.model.controller;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.monese.account.model.domain.Currency;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.UUID;

public class AccountTransferRequest {
    @NotNull
    private final UUID sourceAccountId;
    @NotNull
    private final UUID destinationAccountId;
    @NotNull
    private final Long amount;
    @NotNull
    private final Currency currency;
    @NotEmpty
    private final String reference;

    @JsonCreator
    public AccountTransferRequest(
            @JsonProperty("sourceAccountId") UUID sourceAccountId,
            @JsonProperty("destinationAccountId") UUID destinationAccountId,
            @JsonProperty("amount") Long amount,
            @JsonProperty("currency") Currency currency,
            @JsonProperty("reference") String reference) {
        this.sourceAccountId = sourceAccountId;
        this.destinationAccountId = destinationAccountId;
        this.amount = amount;
        this.currency = currency;
        this.reference = reference;
    }

    public UUID getSourceAccountId() {
        return sourceAccountId;
    }

    public UUID getDestinationAccountId() {
        return destinationAccountId;
    }

    public Long getAmount() {
        return amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public String getReference() {
        return reference;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountTransferRequest that = (AccountTransferRequest) o;
        return Objects.equals(sourceAccountId, that.sourceAccountId) && Objects.equals(destinationAccountId, that.destinationAccountId) && Objects.equals(amount, that.amount) && currency == that.currency && Objects.equals(reference, that.reference);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sourceAccountId, destinationAccountId, amount, currency, reference);
    }
}
