package com.monese.account.model.controller;

import com.monese.account.model.domain.Money;

import java.time.LocalDate;
import java.util.Objects;
import java.util.UUID;

public class StatementItem {
    private final UUID id;
    private final Money amount;
    private final String reference;
    private final LocalDate valueDate;

    public StatementItem(UUID id, Money amount, String reference, LocalDate valueDate) {
        this.id = id;
        this.amount = amount;
        this.reference = reference;
        this.valueDate = valueDate;
    }

    public UUID getId() {
        return id;
    }

    public Money getAmount() {
        return amount;
    }

    public String getReference() {
        return reference;
    }

    public LocalDate getValueDate() {
        return valueDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StatementItem that = (StatementItem) o;
        return Objects.equals(id, that.id) && Objects.equals(amount, that.amount) && Objects.equals(reference, that.reference) && Objects.equals(valueDate, that.valueDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, amount, reference, valueDate);
    }
}
