package com.monese.account.model.controller;

import com.monese.account.model.domain.Money;

import java.util.List;
import java.util.Objects;

public class StatementResponse {
    private final Money balance;
    private final List<StatementItem> transactions;

    public StatementResponse(Money balance, List<StatementItem> transactions) {
        this.balance = balance;
        this.transactions = transactions;
    }

    public Money getBalance() {
        return balance;
    }

    public List<StatementItem> getTransactions() {
        return transactions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StatementResponse that = (StatementResponse) o;
        return Objects.equals(balance, that.balance) && Objects.equals(transactions, that.transactions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(balance, transactions);
    }
}
