package com.monese.account.model.entity;

import com.monese.account.model.domain.Currency;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

@Entity
public class AccountTransaction {
    @Id
    private UUID id;

    private UUID accountId;

    private BigDecimal amount;

    private Currency currency;

    private String reference;

    private LocalDateTime valueDate;

    public AccountTransaction() {
    }

    public AccountTransaction(UUID id, UUID accountId, BigDecimal amount, Currency currency, String reference, LocalDateTime valueDate) {
        this.id = id;
        this.accountId = accountId;
        this.amount = amount;
        this.currency = currency;
        this.reference = reference;
        this.valueDate = valueDate;
    }

    public UUID getId() {
        return id;
    }

    public UUID getAccountId() {
        return accountId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public String getReference() {
        return reference;
    }

    public LocalDateTime getValueDate() {
        return valueDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountTransaction that = (AccountTransaction) o;
        return Objects.equals(id, that.id) && Objects.equals(accountId, that.accountId) && Objects.equals(amount, that.amount) && currency == that.currency && Objects.equals(reference, that.reference) && Objects.equals(valueDate, that.valueDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, accountId, amount, currency, reference, valueDate);
    }
}
