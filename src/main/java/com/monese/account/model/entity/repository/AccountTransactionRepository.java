package com.monese.account.model.entity.repository;

import com.monese.account.model.entity.AccountTransaction;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

public interface AccountTransactionRepository extends CrudRepository<AccountTransaction, UUID> {

    List<AccountTransaction> findAllByAccountId(UUID accountId);
}
