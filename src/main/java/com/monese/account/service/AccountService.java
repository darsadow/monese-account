package com.monese.account.service;

import com.monese.account.model.controller.StatementResponse;
import com.monese.account.model.domain.Money;

import java.util.UUID;

public interface AccountService {
    StatementResponse accountStatement(UUID accountId);
    void transfer(UUID sourceAccountId, UUID destinationAccountId, Money amount, String reference);
}
