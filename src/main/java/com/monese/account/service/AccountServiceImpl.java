package com.monese.account.service;

import com.monese.account.model.controller.StatementItem;
import com.monese.account.model.controller.StatementResponse;
import com.monese.account.model.domain.Money;
import com.monese.account.service.exception.AccountNotFoundException;
import com.monese.account.service.exception.InsufficientFundsException;
import com.monese.account.service.exception.InvalidTransferCurrencyException;
import com.monese.account.model.entity.Account;
import com.monese.account.model.entity.AccountTransaction;
import com.monese.account.model.entity.repository.AccountRepository;
import com.monese.account.model.entity.repository.AccountTransactionRepository;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;
    private final AccountTransactionRepository accountTransactionRepository;
    private final UUIDProvider uuidProvider;
    private final LocalDateTimeProvider localDateTimeProvider;

    public AccountServiceImpl(
            AccountRepository accountRepository,
            AccountTransactionRepository accountTransactionRepository,
            UUIDProvider uuidProvider,
            LocalDateTimeProvider localDateTimeProvider
    ) {
        this.accountRepository = accountRepository;
        this.accountTransactionRepository = accountTransactionRepository;
        this.uuidProvider = uuidProvider;
        this.localDateTimeProvider = localDateTimeProvider;
    }

    @Override
    public StatementResponse accountStatement(UUID accountId) {
        Account account = accountRepository.findById(accountId)
                .orElseThrow(AccountNotFoundException::new);

        BigDecimal balance = account.getBalance();
        List<StatementItem> transactions = accountTransactionRepository.findAllByAccountId(accountId)
                .stream()
                .map(accountTransaction -> new StatementItem(
                        accountTransaction.getId(),
                        new Money(accountTransaction.getAmount(), accountTransaction.getCurrency()),
                        accountTransaction.getReference(),
                        accountTransaction.getValueDate().toLocalDate()
                ))
                .collect(Collectors.toList());

        return new StatementResponse(new Money(balance, account.getCurrency()), transactions);
    }

    @Override
    @Transactional
    public void transfer(UUID sourceAccountId, UUID destinationAccountId, Money amount, String reference) {
        Account sourceAccount = accountRepository.findById(sourceAccountId)
                .orElseThrow(AccountNotFoundException::new);
        Account destinationAccount = accountRepository.findById(destinationAccountId)
                .orElseThrow(AccountNotFoundException::new);

        if (sourceAccount.getCurrency() != amount.getCurrency() || destinationAccount.getCurrency() != amount.getCurrency()) {
            throw new InvalidTransferCurrencyException();
        }

        BigDecimal sourceAccountBalance = sourceAccount.getBalance();

        if (sourceAccountBalance.compareTo(amount.getAmount()) < 0) {
            throw new InsufficientFundsException();
        }

        LocalDateTime transactionTime = localDateTimeProvider.now();

        AccountTransaction sourceTransaction = new AccountTransaction(
                uuidProvider.randomUUID(),
                sourceAccountId,
                amount.getAmount().multiply(new BigDecimal(-1)),
                sourceAccount.getCurrency(),
                reference,
                transactionTime
        );

        AccountTransaction destinationTransaction = new AccountTransaction(
                uuidProvider.randomUUID(),
                destinationAccountId,
                amount.getAmount(),
                sourceAccount.getCurrency(),
                reference,
                transactionTime
        );

        BigDecimal newSourceAccountBalance = sourceAccount.getBalance().subtract(amount.getAmount());
        BigDecimal newDestinationAccountBalance = destinationAccount.getBalance().add(amount.getAmount());

        sourceAccount.setBalance(newSourceAccountBalance);
        destinationAccount.setBalance(newDestinationAccountBalance);

        accountTransactionRepository.save(sourceTransaction);
        accountTransactionRepository.save(destinationTransaction);
        accountRepository.save(sourceAccount);
        accountRepository.save(destinationAccount);

    }
}
