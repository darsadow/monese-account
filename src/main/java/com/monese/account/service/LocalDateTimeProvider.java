package com.monese.account.service;

import java.time.LocalDateTime;

public interface LocalDateTimeProvider {
    LocalDateTime now();
}
