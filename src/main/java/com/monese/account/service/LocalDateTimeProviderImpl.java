package com.monese.account.service;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class LocalDateTimeProviderImpl implements LocalDateTimeProvider {
    @Override
    public LocalDateTime now() {
        return LocalDateTime.now();
    }
}
