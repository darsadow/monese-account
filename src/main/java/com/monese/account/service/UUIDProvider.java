package com.monese.account.service;

import java.util.UUID;

public interface UUIDProvider {
    UUID randomUUID();
}
