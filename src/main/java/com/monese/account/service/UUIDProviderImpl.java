package com.monese.account.service;

import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class UUIDProviderImpl implements UUIDProvider {
    public UUID randomUUID() {
        return UUID.randomUUID();
    }
}
