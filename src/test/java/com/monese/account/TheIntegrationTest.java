package com.monese.account;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.monese.account.model.controller.AccountTransferRequest;
import com.monese.account.model.domain.Currency;
import com.monese.account.model.entity.Account;
import com.monese.account.model.entity.repository.AccountRepository;
import com.monese.account.testcontainers.PostgresContainer;
import org.junit.ClassRule;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.isA;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ContextConfiguration(initializers = PostgresContainer.DockerPostgreDataSourceInitializer.class)
public class TheIntegrationTest {

    @ClassRule
    public static PostgresContainer container = PostgresContainer.getInstance();

    static {
        container.start();
    }

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void itMakesTransferAndChecksBalanceAndTransactionHistory() throws Exception {
        Account sourceAccount = new Account(UUID.randomUUID(), BigDecimal.valueOf(1000), Currency.GBP);
        Account destinationAccount = new Account(UUID.randomUUID(), BigDecimal.ZERO, Currency.GBP);

        accountRepository.save(sourceAccount);
        accountRepository.save(destinationAccount);

        mockMvc.perform(
                post("/accounts/transfer")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(asJsonString(new AccountTransferRequest(
                            sourceAccount.getId(),
                            destinationAccount.getId(),
                            345L,
                            Currency.GBP,
                            "Test reference"
                    )))
        )
                .andExpect(status().isAccepted());

        mockMvc.perform(
                get("/accounts/" + sourceAccount.getId().toString() + "/statement")
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.balance.amount", is(655.0)))
                .andExpect(jsonPath("$.balance.currency", is("GBP")))
        .andExpect(jsonPath("$.transactions.*", isA(ArrayList.class)))
        .andExpect(jsonPath("$.transactions.*", hasSize(1)))
        .andExpect(jsonPath("$.transactions[0].amount.amount", is(-345.0)))
        .andExpect(jsonPath("$.transactions[0].amount.currency", is("GBP")))
        .andExpect(jsonPath("$.transactions[0].reference", is("Test reference")));

        mockMvc.perform(
                get("/accounts/" + destinationAccount.getId().toString() + "/statement")
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.balance.amount", is(345.0)))
                .andExpect(jsonPath("$.balance.currency", is("GBP")))
        .andExpect(jsonPath("$.transactions.*", isA(ArrayList.class)))
        .andExpect(jsonPath("$.transactions.*", hasSize(1)))
        .andExpect(jsonPath("$.transactions[0].amount.amount", is(345.0)))
        .andExpect(jsonPath("$.transactions[0].amount.currency", is("GBP")))
        .andExpect(jsonPath("$.transactions[0].reference", is("Test reference")));
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
